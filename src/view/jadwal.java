/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import helper.Koneksi;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.table.DefaultTableModel;
import static view.asisten.baris;


public class jadwal extends javax.swing.JFrame {

    
    String[] hari = {"Senin" , "Selesa" , "Rabu" , "Kamis" ,"Jumat" , "Sabtu", "Ahad"};
    
    Connection koneksi;
    private DefaultTableModel tableModel;
    static int baris;
        
    
    public jadwal() throws Exception {
     
        initComponents();
         setSpinnerJamMulai();
        setSpinnerJamSelesai();
        setHari();
        bUpdate.setEnabled(false);
        bEdit.setEnabled(false);
        bHapus.setEnabled(false);
        
        Koneksi konek = new Koneksi();
        koneksi  = konek.koneksi;
        isiData();
        isiComboIDmataPrak();
        isiComboAsisten1();
        isiComboAsisten2();
       
     
    }
    
    public void bersihkanField(){
        
        jTextField1.setText("");
        
        jComboBox1.setSelectedIndex(0);
        jComboBox2.setSelectedIndex(0);
        jComboBox3.setSelectedIndex(0);
         jComboBox4.setSelectedIndex(0);
        
    }
    
    
    public void isiComboIDmataPrak(){
        
        jComboBox2.removeAllItems();
        
        
        try{
                PreparedStatement st;
        
                String query = "select * from matapraktikum";
                st = koneksi.prepareStatement(query);
                ResultSet rs = st.executeQuery();

                while(rs.next()){
                 
                    jComboBox2.addItem(rs.getString("ID_matapraktikum"));
                    
                 }
        }catch(Exception e){
            e.printStackTrace();
        }
                
        
    }
    
    
    public void isiComboAsisten1(){
        
        jComboBox3.removeAllItems();
        
        
        try{
                PreparedStatement st;
        
                String query = "SELECT ID_asisten FROM `asisten`";
                st = koneksi.prepareStatement(query);
                ResultSet rs = st.executeQuery();

                while(rs.next()){
                 
                    jComboBox3.addItem(rs.getString("ID_asisten"));
                    
                 }
        }catch(Exception e){
            e.printStackTrace();
        }
                
        
    }

    public void isiComboAsisten2(){
        
        jComboBox4.removeAllItems();
        
        
        try{
                PreparedStatement st;
        
                String query = "SELECT ID_asisten FROM `asisten`";
                st = koneksi.prepareStatement(query);
                ResultSet rs = st.executeQuery();

                while(rs.next()){
                 
                    jComboBox4.addItem(rs.getString("ID_asisten"));
                    
                 }
        }catch(Exception e){
            e.printStackTrace();
        }
                
        
    }
    
    public void setSpinnerJamMulai(){
        SpinnerModel model = new SpinnerDateModel();
        //JSpinner timeSpinner = new JSpinner(model);
        jSpinner1.setModel(model);
        JComponent editor = new JSpinner.DateEditor(jSpinner1, "HH:mm");
        jSpinner1.setEditor(editor);
    }
    
    
    public void setSpinnerJamSelesai(){
        SpinnerModel model = new SpinnerDateModel();
        //JSpinner timeSpinner = new JSpinner(model);
        jSpinner2.setModel(model);
        JComponent editor = new JSpinner.DateEditor(jSpinner2, "HH:mm");
        jSpinner2.setEditor(editor);

    }
    
    public void setHari(){
        
        jComboBox1.removeAllItems();
        
        for( String hr : hari){
            
            jComboBox1.addItem(hr);
            
        }
        
    }
    
    
    
    public void isiData(){
        
       tableModel = new DefaultTableModel();
        jTable1.setModel(tableModel);
        tableModel.addColumn("ID Jadwal");
        tableModel.addColumn("Jam Mulai");
        tableModel.addColumn("Jam Selesai");
        tableModel.addColumn("Frekuensi");
        tableModel.addColumn("Hari");
        tableModel.addColumn("ID Mata Praktikum");
        tableModel.addColumn("Asisten 1");
        tableModel.addColumn("Asisten 2");
        
        try{
        PreparedStatement st;
        
        String query = "select * from jadwal";
        st = koneksi.prepareStatement(query);
        ResultSet rs = st.executeQuery();
        while(rs.next()){
            Object[] row = new Object[8];
            row[0] = rs.getString("ID_jadwal");
            row[1] = rs.getString("Jam_mulai");
            row[2] = rs.getString("Jam_selesai");
            row[3] = rs.getString("Frekuensi");
            row[4] = rs.getString("Hari");
            row[5] = rs.getString("ID_matapraktikum");
            row[6] = rs.getString("Asisten_1");
            row[7] = rs.getString("Asisten_2");
            
            tableModel.addRow(row);    
         }

        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    
    
    
    
    public void updateData(){
            for(int a = 0 ; a< tableModel.getRowCount(); a++){
            tableModel.removeRow(a);
        }
        try{
      
        
        PreparedStatement st;
        
        String query = "update jadwal set   Jam_mulai=? , Jam_selesai=?, Frekuensi=?, Hari=?, ID_matapraktikum=?, Asisten_1=? , Asisten_2=? where ID_jadwal=?";
        st = koneksi.prepareStatement(query);
        st.setString(8, jTextField1.getText());
        st.setString(1, new SimpleDateFormat("HH:mm").format(jSpinner1.getValue()));
        st.setString(2, new SimpleDateFormat("HH:mm").format(jSpinner2.getValue()));
        st.setString(3, jTextField2.getText());
        st.setString(4, jComboBox1.getSelectedItem().toString());
        st.setString(5, jComboBox2.getSelectedItem().toString());
        st.setString(6, jComboBox3.getSelectedItem().toString());
        st.setString(7, jComboBox4.getSelectedItem().toString());
        st.execute();
        
        JOptionPane.showMessageDialog(null, "Berhasil Mengupdate data");
        }catch(Exception e){
            e.printStackTrace();
            
            JOptionPane.showMessageDialog(null, "Gagal Mengupdate data");
        }
        
        
    }
    
    
    public void hapus(){
        
        tableModel.removeRow(baris);
        try{
        String query = "delete from jadwal where ID_jadwal= ?";
        PreparedStatement st = koneksi.prepareStatement(query);
        st.setString(1, jTextField1.getText());
        st.executeUpdate();
        JOptionPane.showMessageDialog(null, "Berhasil Menghapus data");
     
        
        }catch(Exception e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Gagal Menghapus data");
        }
    }
    
    
    public void simpan(){
        try{
        String query = "insert into jadwal values (?,?,?,?,?,?,?,?)";
        PreparedStatement st = koneksi.prepareCall(query);
        st.setString(1, jTextField1.getText());
        st.setString(2, new SimpleDateFormat("HH:mm").format(jSpinner1.getValue()));
        st.setString(3, new SimpleDateFormat("HH:mm").format(jSpinner2.getValue()));
        st.setString(4, jTextField2.getText());
        st.setString(5, jComboBox1.getSelectedItem().toString());
        st.setString(6, jComboBox2.getSelectedItem().toString());
        st.setString(7, jComboBox3.getSelectedItem().toString());
        st.setString(8, jComboBox4.getSelectedItem().toString());
        st.execute();
        
        JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan");
        
            
        
        }catch(Exception e){
            e.printStackTrace();
               JOptionPane.showMessageDialog(null, "Data Gagal Disimpan");
        }
                
        
        
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        jComboBox4 = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        jSpinner1 = new javax.swing.JSpinner();
        jSpinner2 = new javax.swing.JSpinner();
        jPanel1 = new javax.swing.JPanel();
        bUpdate = new javax.swing.JButton();
        bHapus = new javax.swing.JButton();
        bEdit = new javax.swing.JButton();
        bSimpan = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Menu Input Data Jadwal");

        jLabel4.setText("ID Jadwal");

        jTextField1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField1MouseClicked(evt);
            }
        });

        jLabel5.setText("Jam Mulai");

        jLabel6.setText("Jam Selesai");

        jLabel7.setText("Hari");

        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel8.setText("ID Mata Praktikum");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel9.setText("Asisten 1");

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel10.setText("Asisten 2");

        jComboBox4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel11.setText("Daftar Jadwal Yang Telah Di Input");

        jSpinner1.setModel(new javax.swing.SpinnerDateModel(new java.util.Date(), null, null, java.util.Calendar.HOUR));

        jSpinner2.setModel(new javax.swing.SpinnerDateModel(new java.util.Date(), null, null, java.util.Calendar.HOUR));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        bUpdate.setText("Update");
        bUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bUpdateActionPerformed(evt);
            }
        });

        bHapus.setText("Hapus");
        bHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bHapusActionPerformed(evt);
            }
        });

        bEdit.setText("Edit");
        bEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEditActionPerformed(evt);
            }
        });

        bSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/save.png"))); // NOI18N
        bSimpan.setText("Simpan");
        bSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSimpanActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(bSimpan, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(bHapus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bSimpan)
                    .addComponent(bEdit))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bHapus)
                    .addComponent(bUpdate))
                .addContainerGap())
        );

        jLabel2.setText("Frekuensi");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel10)
                                .addComponent(jLabel9)
                                .addComponent(jLabel8)
                                .addComponent(jLabel7)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jComboBox4, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jComboBox3, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jComboBox2, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextField1, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jComboBox1, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel5)
                                            .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(45, 45, 45)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel6))))
                                .addComponent(jLabel4)
                                .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel2)
                            .addComponent(jTextField2))
                        .addGap(65, 65, 65)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addContainerGap())))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(63, 63, 63)
                        .addComponent(jLabel11)
                        .addGap(14, 14, 14)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(81, 81, 81))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSimpanActionPerformed
       simpan();
        isiData();
        bersihkanField();
    }//GEN-LAST:event_bSimpanActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        bEdit.setEnabled(true);
        bSimpan.setEnabled(false);
    }//GEN-LAST:event_jTable1MouseClicked

    private void bEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEditActionPerformed
         
        baris = jTable1.getSelectedRow();
        System.out.println(baris);
        jTextField1.setText(jTable1.getValueAt(baris, 0).toString());
        jTextField2.setText(jTable1.getValueAt(baris, 3).toString());
        jComboBox1.setSelectedItem(jTable1.getValueAt(baris, 4));
        jComboBox2.setSelectedItem(jTable1.getValueAt(baris, 5));
        jComboBox3.setSelectedItem(jTable1.getValueAt(baris, 6));
        jComboBox4.setSelectedItem(jTable1.getValueAt(baris, 7));
        
        bSimpan.setEnabled(false);
        bEdit.setEnabled(false);
        bHapus.setEnabled(true);
        bUpdate.setEnabled(true);
    }//GEN-LAST:event_bEditActionPerformed

    private void bUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bUpdateActionPerformed
        updateData();
        isiData();
        bersihkanField();
        bUpdate.setEnabled(false);
        bHapus.setEnabled(false);
        bSimpan.setEnabled(true);
    }//GEN-LAST:event_bUpdateActionPerformed

    private void bHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bHapusActionPerformed
       hapus();
        isiData();
        bersihkanField();
        bHapus.setEnabled(false);
        bUpdate.setEnabled(false);
    }//GEN-LAST:event_bHapusActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jTextField1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField1MouseClicked
         bSimpan.setEnabled(true);
        bEdit.setEnabled(false);
    }//GEN-LAST:event_jTextField1MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jadwal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jadwal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jadwal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jadwal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new jadwal().setVisible(true);
                } catch (Exception ex) {
                    Logger.getLogger(jadwal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bEdit;
    private javax.swing.JButton bHapus;
    private javax.swing.JButton bSimpan;
    private javax.swing.JButton bUpdate;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JComboBox<String> jComboBox4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JSpinner jSpinner2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
