package view;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import helper.Koneksi;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

public class absensimhs extends javax.swing.JFrame implements SerialPortEventListener{
    Connection koneksi;
    String id_jadwal;
    ArrayList<String> daftar_kartu = new ArrayList<>();
    String no_kartu;
    
    //Counter untuk di jadikan acuan ketika ada asisten yang telah absen;
    int counterAbsenAsisten = 0;
    
    public absensimhs() throws Exception {
        initComponents();
         Koneksi konek = new Koneksi();
        koneksi  = konek.koneksi;
       
        
        
        initialize();
	Thread t=new Thread() {
		public void run() {
			try {Thread.sleep(1000000);} catch (InterruptedException ie) {}
		}
	};
	t.start();
	System.out.println("Silahkan Scan Kartu Anda");
    }
    
    
    
    SerialPort serialPort;
    public static String inputline;
        /** The port we're normally going to use. */
	private static final String PORT_NAMES[] = { 
			"/dev/tty.usbserial-A9007UX1", // Mac OS X
                        "/dev/ttyACM0", // Raspberry Pi
			"/dev/ttyUSB0", // Linux
			"COM3", // Windows
	};
	private BufferedReader input;
        private PrintWriter myoutput;
        private PrintStream printStream;
        /** The output stream to the port */
	private OutputStream output;
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;
	/** Default bits per second for COM port. */
	private static final int DATA_RATE = 9600;

	public void initialize() {
                
                System.setProperty("gnu.io.xtx.SerialPorts", "COM3");

		CommPortIdentifier portId = null;
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

		//First, Find an instance of serial port as set in PORT_NAMES.
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			for (String portName : PORT_NAMES) {
				if (currPortId.getName().equals(portName)) {
					portId = currPortId;
					break;
				}
			}
		}
		if (portId == null) {
			System.out.println("Could not find COM port.");
			return;
		}

		try {
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(),
					TIME_OUT);

			// set port parameters
			serialPort.setSerialPortParams(DATA_RATE,
					SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);

			// open the streams
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			output = serialPort.getOutputStream();
                        printStream = new PrintStream(output);
			// add event listeners
			serialPort.addEventListener((SerialPortEventListener) this);
			serialPort.notifyOnDataAvailable(true);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}

	/**
	 * This should be called when you stop using the port.
	 * This will prevent port locking on platforms like Linux.
	 */
	public synchronized void close() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

	/**
	 * Handle an event on the serial port. Read the data and print it.
	 */
	public synchronized void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				String inputLine=input.readLine();
                             
				String inputLine2=input.readLine();
                                //pengaturan pembaccan ID
                                   if(inputLine.contains("ID")){
                                    
                                    String id = inputLine.substring(inputLine.lastIndexOf("Kartu:") + 6 );
                                    //jTextField1.setText(id);
                                    if(inputLine2.toString().trim().equals("0")){
                                    
                                        absenAsisten(id);
                               
                              
                                    }else if(inputLine2.toString().trim().equals("1")){
                                       
                                        absenMahasiswa(id);
                                    
                                        
                                    
                                    }
                                  }

                                
                                System.out.println(inputLine);
                                System.out.println(inputLine2);
			} catch (Exception e) {
				System.err.println(e.toString());
			}
		}
		// Ignore all the other eventTypes, but you should consider the other ones.
	}
    
   
    
    
 public String tentukanHari(String hari){
       switch(hari){
                    
                    case "Sunday":
                        return "Ahad";
                        
                       
                    case "Monday":
                        return "Senin";
                        
                    case "Tuesday":
                        return "Selasa";
                     
                    case "Wednesday":
                        return "Rabu";
                        
                        
                    case "Thursday":
                        return "Kamis";
                        
                    case "Friday":
                        return "Jumat";
                        
                    case "Saturday":
                        return "Sabtu";
                    
                    default :
                        
                   
                        return "";
                        
                }
 }
    
 public void absenMahasiswa (String nomor){
     try{
                PreparedStatement st;
                String jam = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
                String hari = new SimpleDateFormat("EEEE").format(Calendar.getInstance().getTime());
                System.out.println(jam +" "+hari);
                
                hari = tentukanHari(hari);
                System.out.println(hari);
                String query = "SELECT * from jadwal where Hari=? and Jam_mulai<=? and Jam_selesai>=?";
                st = koneksi.prepareStatement(query);
                st.setString(1,hari);
                st.setString(2, jam);
                st.setString(3,jam);
                ResultSet rs = st.executeQuery();
         
            while(rs.next()){
                id_jadwal = rs.getString("ID_jadwal");
            }
            
            String nomor_kartu = "SELECT mahasiswa.No_kartu , mahasiswa.Nama, mahasiswa.Stambuk , absen.ID_absen from absen,mahasiswa where ID_jadwal=? and absen.Stambuk=mahasiswa.Stambuk";
            PreparedStatement st_nomor_kartu = koneksi.prepareStatement(nomor_kartu);
            st_nomor_kartu.setString(1,id_jadwal);
            ResultSet rs_nomor_kartu = st_nomor_kartu.executeQuery();

            daftar_kartu.clear();
            while(rs_nomor_kartu.next()){
                
                
               // daftar_kartu.add(rs_nomor_kartu.getString("No_kartu"));
                
               
                System.out.println();
                String id = nomor.toString().trim();
                String noKartu = rs_nomor_kartu.getString("No_kartu").toString().trim();
                System.out.println("This is "+id);
                if(noKartu.equals(id)){
                    
                    System.out.println("Check");
                    
                    String idAbsen = rs_nomor_kartu.getString("ID_absen");
                    String waktu = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
               
                    //String update_absen = "INSERT INTO absensi_mhs (ID_absen , waktu , Status) values (?,?,?)";
                    //Kita ganti menjadi proses update
                    String update_absen = "UPDATE absensi_mhs set Status=? where ID_absen=? and waktu=?";
                    
                    PreparedStatement stUpdateAbsen  = koneksi.prepareStatement(update_absen);
                    stUpdateAbsen.setString(1, "Hadir" );
                    stUpdateAbsen.setString(2, idAbsen);
                    stUpdateAbsen.setString(3, waktu);
                    stUpdateAbsen.execute();
                    String stb = rs_nomor_kartu.getString("Stambuk");
                    printStream.print("Absensi Berhasil    ");
                    printStream.print("Silahkan Masuk   ");
                    printStream.print("    "+stb+"     ");         
                }
            }
   
        }catch(Exception e){
            e.printStackTrace();
        }
 }
 
 public void absenAsisten (String nomor){
     try{
                PreparedStatement st;
                String jam = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
                String hari = new SimpleDateFormat("EEEE").format(Calendar.getInstance().getTime());
                System.out.println(jam +" "+hari);
                
                hari = tentukanHari(hari);
                System.out.println(hari);
                String query = "SELECT * from jadwal where Hari=? and Jam_mulai<=? and Jam_selesai>=?";
                st = koneksi.prepareStatement(query);
                st.setString(1,hari);
                st.setString(2, jam);
                st.setString(3,jam);
                ResultSet rs = st.executeQuery();
              
               
            while(rs.next()){
                id_jadwal = rs.getString("ID_jadwal");
            }
            
            String nomor_kartu = "SELECT asisten.No_kartu , asisten.Nama, absen_asisten.ID_absen_asisten from absen_asisten, asisten where ID_jadwal=? and absen_asisten.ID_asisten=asisten.ID_asisten";
            PreparedStatement st_nomor_kartu = koneksi.prepareStatement(nomor_kartu);
            st_nomor_kartu.setString(1,id_jadwal);
            ResultSet rs_nomor_kartu = st_nomor_kartu.executeQuery();
            
            
            daftar_kartu.clear();
            counterAbsenAsisten = 0;
            while(rs_nomor_kartu.next()){
                
                
               // daftar_kartu.add(rs_nomor_kartu.getString("No_kartu"));
                
               
                System.out.println();
                String id = nomor.toString().trim();
                String noKartu = rs_nomor_kartu.getString("No_kartu").toString().trim();
                System.out.println("This is "+id);
              
                if(noKartu.equals(id)){
                    counterAbsenAsisten++;
                    System.out.println("Check");
                    
                    String idAbsen = rs_nomor_kartu.getString("ID_absen");
                    String waktu = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
               
                    String update_absen = "INSERT INTO absensi_asisten (ID_absensi_asisten , waktu , Status) values (?,?,?)";
                    PreparedStatement stUpdateAbsen  = koneksi.prepareStatement(update_absen);
                    stUpdateAbsen.setString(1, idAbsen);
                    stUpdateAbsen.setString(2, waktu);
                    stUpdateAbsen.setString(3, "Hadir");
                    
                    stUpdateAbsen.execute();
                    String stb = rs_nomor_kartu.getString("Stambuk");
                    printStream.print("Absensi Berhasil    ");
                    printStream.print("   Silahkan Masuk   ");
                    printStream.print("    "+stb+"     ");        
                    
                    if(counterAbsenAsisten == 1){
                        
                        
                        
                    }
                    
             
                }
              
                
            }
   
        }catch(Exception e){
            e.printStackTrace();
        }
 }
 
 
 public void isimahasiswa(){
       try{
                PreparedStatement st;
                String jam = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
                String hari = new SimpleDateFormat("EEEE").format(Calendar.getInstance().getTime());
                System.out.println(jam +" "+hari);
                
                hari = tentukanHari(hari);
                System.out.println(hari);
                String query = "SELECT * from jadwal where Hari=? and Jam_mulai<=? and Jam_selesai>=?";
                st = koneksi.prepareStatement(query);
                st.setString(1,hari);
                st.setString(2, jam);
                st.setString(3,jam);
                ResultSet rs = st.executeQuery();
              
               
            while(rs.next()){
                id_jadwal = rs.getString("ID_jadwal");
            }
            
            String nomor_kartu = "SELECT mahasiswa.No_kartu , mahasiswa.Nama, mahasiswa.Stambuk , absen.ID_absen from absen,mahasiswa where ID_jadwal=? and absen.Stambuk=mahasiswa.Stambuk";
            PreparedStatement st_nomor_kartu = koneksi.prepareStatement(nomor_kartu);
            st_nomor_kartu.setString(1,id_jadwal);
            ResultSet rs_nomor_kartu = st_nomor_kartu.executeQuery();
            
            
            daftar_kartu.clear();
            while(rs_nomor_kartu.next()){
                
                
                    String idAbsen = rs_nomor_kartu.getString("ID_absen");
                    String waktu = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
               
                    String update_absen = "INSERT INTO absensi_mhs (ID_absen , waktu , Status) values (?,?,?)";
                    PreparedStatement stUpdateAbsen  = koneksi.prepareStatement(update_absen);
                    stUpdateAbsen.setString(1, idAbsen);
                    stUpdateAbsen.setString(2, waktu);
                    stUpdateAbsen.setString(3, "Tidak Hadir");
                    
                    stUpdateAbsen.execute();
                   
                
              
                
            }
   
        }catch(Exception e){
            e.printStackTrace();
        }
 }

 
 public void jadwal (){
     try {
          PreparedStatement st;
          String query = "SELECT Stambuk, ID_jadwal, Hari FROM `absen` where ";
          st = koneksi.prepareStatement(query);
          
          
          
     } catch (Exception e) {
     }
     
     
     
     
 }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 914, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 499, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(absensimhs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(absensimhs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(absensimhs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(absensimhs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new absensimhs().setVisible(true);
                } catch (Exception ex) {
                    Logger.getLogger(absensimhs.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
