
package view;

import java.beans.Visibility;
import java.util.Date;
import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;

public class Home extends javax.swing.JFrame {
    
    public Home() {
        initComponents();
        
        
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem_mahasiswa = new javax.swing.JMenuItem();
        jMenuItem_asisten = new javax.swing.JMenuItem();
        jMenuItem_jadwal = new javax.swing.JMenuItem();
        jMenuItem_matapraktikum = new javax.swing.JMenuItem();
        jMenuItem_laboratorium = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jMenu3.setText("File");

        jMenuItem9.setText("Home");
        jMenu3.add(jMenuItem9);

        jMenuItem10.setText("Pengaturan");
        jMenu3.add(jMenuItem10);

        jMenuItem7.setText("Exit");
        jMenu3.add(jMenuItem7);

        jMenuBar1.add(jMenu3);

        jMenu1.setText("Data");

        jMenuItem_mahasiswa.setText("Data Mahasiswa");
        jMenuItem_mahasiswa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_mahasiswaActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem_mahasiswa);

        jMenuItem_asisten.setText("Data Asisten");
        jMenuItem_asisten.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_asistenActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem_asisten);

        jMenuItem_jadwal.setText("Data Jadwal");
        jMenuItem_jadwal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_jadwalActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem_jadwal);

        jMenuItem_matapraktikum.setText("Data Mata Praktikum");
        jMenuItem_matapraktikum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_matapraktikumActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem_matapraktikum);

        jMenuItem_laboratorium.setText("Data Laboratorium");
        jMenuItem_laboratorium.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_laboratoriumActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem_laboratorium);

        jMenuBar1.add(jMenu1);

        jMenu6.setText("Monitoring");
        jMenuBar1.add(jMenu6);

        jMenu5.setText("Registrasi");

        jMenuItem5.setText("Mahasiswa");
        jMenu5.add(jMenuItem5);

        jMenuItem6.setText("Asisten");
        jMenu5.add(jMenuItem6);

        jMenuBar1.add(jMenu5);

        jMenu2.setText("Tentang");

        jMenuItem3.setText("Bantuan");
        jMenu2.add(jMenuItem3);

        jMenuItem4.setText("Credit");
        jMenu2.add(jMenuItem4);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 835, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 542, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem_mahasiswaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_mahasiswaActionPerformed
        
    }//GEN-LAST:event_jMenuItem_mahasiswaActionPerformed

    private void jMenuItem_jadwalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_jadwalActionPerformed
        
    }//GEN-LAST:event_jMenuItem_jadwalActionPerformed

    private void jMenuItem_asistenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_asistenActionPerformed
        
    }//GEN-LAST:event_jMenuItem_asistenActionPerformed

    private void jMenuItem_matapraktikumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_matapraktikumActionPerformed
        
    }//GEN-LAST:event_jMenuItem_matapraktikumActionPerformed

    private void jMenuItem_laboratoriumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_laboratoriumActionPerformed
        
    }//GEN-LAST:event_jMenuItem_laboratoriumActionPerformed

    public static void main(String args[]) {
    
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Home().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JMenuItem jMenuItem_asisten;
    private javax.swing.JMenuItem jMenuItem_jadwal;
    private javax.swing.JMenuItem jMenuItem_laboratorium;
    private javax.swing.JMenuItem jMenuItem_mahasiswa;
    private javax.swing.JMenuItem jMenuItem_matapraktikum;
    // End of variables declaration//GEN-END:variables
}
